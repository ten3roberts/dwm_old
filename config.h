/* See LICENSE file for copyright and license details. */
#include "fibonacci.c"
#include "gaplessgrid.c"
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 4;

static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "SauceCodePro Nerd Font Mono:size=11:antialias=true" };
static const char dmenufont[]       = "Mononoki Nerd Font Mono:size=12";

static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";

static char termcol0[] = "#000000"; /* black   */
static char termcol1[] = "#ff0000"; /* red     */
static char termcol2[] = "#33ff00"; /* green   */
static char termcol3[] = "#ff0099"; /* yellow  */
static char termcol4[] = "#0066ff"; /* blue    */
static char termcol5[] = "#cc00ff"; /* magenta */
static char termcol6[] = "#00ffff"; /* cyan    */
static char termcol7[] = "#d0d0d0"; /* white   */
static char termcol8[]  = "#808080"; /* black   */
static char termcol9[]  = "#ff0000"; /* red     */
static char termcol10[] = "#33ff00"; /* green   */
static char termcol11[] = "#ff0099"; /* yellow  */
static char termcol12[] = "#0066ff"; /* blue    */
static char termcol13[] = "#cc00ff"; /* magenta */
static char termcol14[] = "#00ffff"; /* cyan    */
static char termcol15[] = "#ffffff"; /* white   */
static char *termcolor[] = {
  termcol0,
  termcol1,
  termcol2,
  termcol3,
  termcol4,
  termcol5,
  termcol6,
  termcol7,
  termcol8,
  termcol9,
  termcol10,
  termcol11,
  termcol12,
  termcol13,
  termcol14,
  termcol15,
};


static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "dev", "web", "media", "sys", "games", "misc", "7", "8", "9" };

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp",     NULL,       NULL,       0,            1,           -1 },
    { "Firefox",  NULL,       NULL,       1 << 1,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",  tile },        /* first entry is default */
    { "><>",  NULL },        /* no layout function means floating behavior */
    { "|\\|", dwindle },
    { "[D]",  deck },
    { "###",  gaplessgrid },
    { "[M]",  monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY, comboview,  {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY, toggleview, {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY, combotag,   {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY, toggletag,  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };


static const char *mutecmd[] =    { "volctrl.sh", "toggle", NULL };
static const char *volupcmd[] =   { "volctrl.sh", "up", NULL };
static const char *voldowncmd[] = { "volctrl.sh", "down", NULL };
/* static const char *miccmd[] =     { "amixer", "set", "Capture", "toggle", NULL }; */

static const char *playpausecmd[] = { "playerctl", "play-pause", NULL };
static const char *playnextcmd[] =  { "playerctl", "next",       NULL };
static const char *playprevcmd[] =  { "playerctl", "previous",   NULL };
static const char *playstopcmd[] =  { "playerctl", "stop",       NULL };

static const char *brupcmd[] =   { "sudo", "xbacklight", "-inc", "10", NULL };
static const char *brdowncmd[] = { "sudo", "xbacklight", "-dec", "10", NULL };

static const char *termcmd[]  =   { "st",      NULL };
static const char *browsercmd[] = { "firefox", NULL };
static const char *filecmd[] =    { "thunar",  NULL };
static const char *spotifycmd[] = { "spotify", NULL };
static const char *discordcmd[] = { "discord", NULL };

static Key keys[] = {
    /* modifier                     key        function        argument */
 	{ MODKEY,              XK_F5,                    xrdb,           {.v = NULL } },
    { MODKEY,              XK_p,                     spawn,          {.v = dmenucmd } },
    { MODKEY,              XK_Return,                spawn,          {.v = termcmd } },

    /* Programs                                                         */
    { MODKEY|ControlMask,  XK_f,                     spawn,          {.v = browsercmd } },
    { MODKEY,              XK_e,                     spawn,          {.v = filecmd } },
    { MODKEY|ControlMask,  XK_s,                     spawn,          {.v = spotifycmd } },
    { MODKEY|ControlMask,  XK_d,                     spawn,          {.v = discordcmd } },


    /* Multimedia */
    { 0,                   XF86XK_AudioMute,         spawn,          {.v = mutecmd } },
    { 0,                   XF86XK_AudioLowerVolume,  spawn,          {.v = voldowncmd } },
    { 0,                   XF86XK_AudioRaiseVolume,  spawn,          {.v = volupcmd } },
    { 0,                   XF86XK_MonBrightnessUp,   spawn,          {.v = brupcmd} },
    { 0,                   XF86XK_MonBrightnessDown, spawn,          {.v = brdowncmd} },

    { 0,                   XF86XK_AudioPlay,         spawn,          {.v = playpausecmd} },
    { 0,                   XF86XK_AudioNext,         spawn,          {.v = playnextcmd} },
    { 0,                   XF86XK_AudioPrev,         spawn,          {.v = playprevcmd} },
    { 0,                   XF86XK_AudioStop,         spawn,          {.v = playstopcmd} },

    { MODKEY|ShiftMask,    XK_b,                     togglebar,      {0} },
    { MODKEY,              XK_j,                     focusstack,     {.i = +1 } },
    { MODKEY,              XK_k,                     focusstack,     {.i = -1 } },

    /* { MODKEY|ShiftMask, XK_j,                     inplacerotate,  {.i = +1} },           */
    /* { MODKEY|ShiftMask, XK_k,                     inplacerotate,  {.i = -1} },           */

    /* Rotate all windows */
    { MODKEY|ShiftMask,    XK_r,                     inplacerotate,  {.i = +2} },
    { MODKEY,              XK_r,                     inplacerotate,  {.i = -2} },

    { MODKEY,              XK_a,                     incnmaster,     {.i = +1 } },
    { MODKEY,              XK_d,                     incnmaster,     {.i = -1 } },

    { MODKEY,              XK_h,                     setmfact,       {.f = -0.05} },
    { MODKEY,              XK_l,                     setmfact,       {.f = +0.05} },

    { MODKEY|ControlMask,  XK_h,                     setcfact,       {.f = -0.25} },
    { MODKEY|ControlMask,  XK_l,                     setcfact,       {.f = +0.25} },
    { MODKEY|ControlMask,  XK_o,                     setcfact,       {.f =  0.00} },

    { MODKEY|ShiftMask,    XK_j,                     pushdown,       {0} },
    { MODKEY|ShiftMask,    XK_k,                     pushup,         {0} },

    { MODKEY,              XK_m,                     focusmaster,    {0} },
    /* I use both,         cause I cannot decide */
    { MODKEY,              XK_g,                     zoom,           {0} },
    { MODKEY,              XK_BackSpace,             zoom,           {0} },

    { MODKEY,              XK_Tab,                   view,           {0} },

    { MODKEY|ShiftMask,    XK_w,                     killclient,     {0} },

    { MODKEY,              XK_t,                     setlayout,      {.v = &layouts[0]} },  /* tall */
    /* { MODKEY,           XK_f,                     setlayout,      {.v = &layouts[1]} },  /1* floating *1/ */
    { MODKEY,              XK_f,                     setlayout,      {.v = &layouts[2]} },  /* fibonacci */
    { MODKEY|ShiftMask,    XK_d,                     setlayout,      {.v = &layouts[3]} },  /* deck */
    { MODKEY|ShiftMask,    XK_g,                     setlayout,      {.v = &layouts[4]} },  /* gaplessgrid */
    { MODKEY|ShiftMask,    XK_m,                     setlayout,      {.v = &layouts[5]} },  /* monocle */

    { MODKEY,              XK_space,                 setlayout,      {0} },
    { MODKEY|ShiftMask,    XK_t,                     togglefloating, {0} },

    { MODKEY|ShiftMask,    XK_f,                     togglefullscr,  {0} },

    { MODKEY,              XK_0,                     view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,    XK_0,                     tag,            {.ui = ~0 } },
    { MODKEY|ControlMask,  XK_0,                     tag,            {.ui = 0 } },

    { MODKEY,              XK_comma,                 focusmon,       {.i = -1 } },
    { MODKEY,              XK_period,                focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,    XK_comma,                 tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,    XK_period,                tagmon,         {.i = +1 } },

    TAGKEYS(                        XK_1, 0)
    TAGKEYS(                        XK_2, 1)
    TAGKEYS(                        XK_3, 2)
    TAGKEYS(                        XK_4, 3)
    TAGKEYS(                        XK_5, 4)
    TAGKEYS(                        XK_6, 5)
    TAGKEYS(                        XK_7, 6)
    TAGKEYS(                        XK_8, 7)
    TAGKEYS(                        XK_9, 8)
    { MODKEY|ShiftMask,                   XK_q,                     quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
